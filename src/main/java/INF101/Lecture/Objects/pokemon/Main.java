package INF101.Lecture.Objects.pokemon;

public class Main {

    public static void main(String[] args) {
        Pokemon pika = new Pokemon("Pikachu", 11);
        Pokemon oddish = new Pokemon("Oddish", 59);

        System.out.println(pika.level);
        pika.levelUp();
        System.out.println(pika.level);
    }
    
}
